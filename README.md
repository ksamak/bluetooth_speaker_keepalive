ever been annoyed by your bluetooth speakers shutting down because of
inactivity?  This little program looks for a bluetooth connection to your audio
mixer, and keeps it on by playing blank audio files periodically.
