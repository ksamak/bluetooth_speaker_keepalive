#!/usr/bin/env python3

"""
Little keepalive for some bluetooth devices that disconnect after 30min or so of inactivity
"""
__author__ = "Ksamak"
__copyright__ = "Copyright 2017, Ksamak"
__credits__ = ["no credit"]
__license__ = "GPL"
__version__ = "3.0+"
__maintainer__ = __author__
__email__ = "ksamak@riseup.net"

import sys
import subprocess
import re
import time
import argparse
import itertools

class BluetoothKeepAlive():
    def __init__(self, args):
        self.args = args

    def findBTDevice(self):
        connections = subprocess.check_output(["hcitool", "con"])
        # matches found hardware addresses
        btAddresses = re.findall("(?:[0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}", connections.decode('UTF-8'))
        sinkList = subprocess.check_output(['pactl', 'list', 'sinks', 'short']).splitlines()
        res = None
        for sink, address in itertools.product(sinkList, btAddresses):
            sink_d = sink.decode('ascii')
            if re.sub(':', '_', address) in sink_d:
                blueSink = sink_d.split("\t")
                print("bluetooth sink ", blueSink[0], " is ", blueSink[-1])
                res = [blueSink[0], address, blueSink[-1]]
                break
        # [PA sink, BT hw address, state]
        if self.args.verbose >= 2 and res:
            print("found connection to bluetooth device ", res[1], " on PA sink ", res[0], ", state: ", res[2])
        return res

    def keepAlive(self):
        try:
            sink, BTDevice, state = self.findBTDevice()
        except:
            # no BT connection, waiting
            return
        if self.args.BTDevice and BTDevice != self.args.BTDevice:
            # found another BT device than requested.
            return
        if state == "SUSPENDED":
            try:
                output = subprocess.check_output(["paplay", "-d", sink, "--volume", str(self.args.volume), self.args.audioFile])
            except subprocess.CalledProcessError as e:
                print(e)
                return
            if self.args.verbose >= 2:
                print("playing file: ", output.decode("UTF-8"))
        elif self.args.verbose >= 2:
            print("sink already alive (", state, "), not interfering")

    def start(self):
        while True:
            self.keepAlive()
            #sleep 10 minutes
            time.sleep(self.args.pollInterval*60)


def parse_args(argv=None):

    parser = argparse.ArgumentParser(description=('Bluetooth keepalive script'), epilog=('Tries to find a bluetooth connection to a sound server (pulse), then keeps it alive when idle to prevent bluetooth disconnection'), formatter_class=lambda prog: argparse.HelpFormatter(prog,max_help_position=5))
    parser.add_argument("--device", action='store', type=str, default=None, dest="BTDevice", help=("specify a device to watch for, eg: '01:23:45:67:89:00' (find this out with 'hcitool con')"))
    parser.add_argument("-v", "--verbose", action="store", type=int, default=2, dest="verbose", choices=range(6), help=("verbosity level"))
    parser.add_argument("-i", "--pollInterval", action="store", type=int, default=10, dest="pollInterval", help=("how often (in minutes) should the program check for active connection"))
    parser.add_argument("-f", "--file", action="store", type=str, default="silent.wav", dest="audioFile", help=("audio file to play when keeping alive"))
    parser.add_argument("--volume", action="store", type=int, default=5, dest="volume", help=("volume when keeping alive (man pactl)"))

    args = parser.parse_args(argv[1:])
    return args

if __name__ == "__main__":
    args = parse_args(sys.argv)
    bka = BluetoothKeepAlive(args)
    bka.start()
